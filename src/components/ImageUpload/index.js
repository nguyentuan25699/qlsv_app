import * as ImagePicker from "expo-image-picker";
import { Block, Button } from "galio-framework";
import React from "react";
import { materialTheme } from "../../../constants";
import { Dimensions, Image } from "react-native";
import defaultImage from "../../img/placeholder.jpg";
const { width } = Dimensions.get("screen");

const ImageUpload = (props) => {
  const [image, setImage] = React.useState(null);
  const [imageResult, setImageResult] = React.useState(null);
  const handleImageChoose = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [4, 4],
      quality: 1,
      base64: true,
    });

    console.log("results", result);

    if (!result.cancelled) {
      setImage(result.uri);
      setImageResult(result);
    }
  };
  React.useEffect(() => {
    (async () => {
      if (Platform.OS !== "web") {
        const { status } =
          await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== "granted") {
          alert("Sorry, we need camera roll permissions to make this work!");
        }
      }
    })();
  }, []);
  React.useEffect(() => {
    setImage(props.avatar);
  }, [props.avatar]);
  React.useEffect(() => {
    props.sendReult(imageResult);
  }, [imageResult]);
  return (
    <Block middle>
      {image && image !== null ? (
        <Image
          source={{ uri: image }}
          style={{ width: width / 2, height: width / 2, borderRadius: 5 }}
        />
      ) : (
        <Image
          source={defaultImage}
          style={{ width: width / 2, height: width / 2, borderRadius: 5 }}
        />
      )}
      <Button color={materialTheme.COLORS.DEFAULT} onPress={handleImageChoose}>
        Chọn ảnh
      </Button>
    </Block>
  );
};

export default ImageUpload;
