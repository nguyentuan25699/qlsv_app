import { Block, Text } from "galio-framework";
import React from "react";
import { Image, TouchableWithoutFeedback } from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import styles from "./styles";
import moment from "moment";
import { BASE_URL_IMAGE } from "../../Service/ServiceURL";

const Event = (props) => {
  const {
    navigation,
    event,
    horizontal,
    onPress,
    full,
    // key,
    numberOfLines,
    style,
    timeColor,
    imageStyle,
  } = props;
  const imageStyles = [
    styles.image,
    full ? styles.fullImage : styles.horizontalImage,
    imageStyle,
  ];
  return (
    <Block
      row={horizontal}
      card
      flex
      style={[styles.event, styles.shadow, style]}
      // key={key}
    >
      <TouchableWithoutFeedback
        style={{ backgroundColor: "yellow" }}
        onPress={onPress}
      >
        <Block flex style={[styles.imageContainer, styles.shadow]}>
          <Image
            source={{ uri: BASE_URL_IMAGE + event.image }}
            style={imageStyles}
          />
        </Block>
      </TouchableWithoutFeedback>
      <TouchableWithoutFeedback
        style={{ backgroundColor: "red" }}
        onPress={onPress}
      >
        <Block flex space="between" style={styles.eventDescription}>
          <Text
            size={14}
            style={styles.eventTitle}
            numberOfLines={numberOfLines}
          >
            {event.name}
          </Text>
          <Text
            size={12}
            muted={!timeColor}
            color={timeColor}
            numberOfLines={1}
          >
            <MaterialCommunityIcons
              name="information-outline"
              color={timeColor}
              size={12}
            />{" "}
            {event.status}
          </Text>
          <Text
            size={12}
            muted={!timeColor}
            color={timeColor}
            numberOfLines={1}
          >
            <MaterialCommunityIcons
              name="map-marker-outline"
              color={timeColor}
              size={12}
            />{" "}
            {event.address}
          </Text>
          <Text
            size={12}
            muted={!timeColor}
            color={timeColor}
            numberOfLines={1}
          >
            <MaterialCommunityIcons
              name="clock-outline"
              color={timeColor}
              size={12}
            />{" "}
            {moment(event.startTime).format("HH:mm DD-MM-yyyy")} -{" "}
            {moment(event.endTime).format("HH:mm DD-MM-yyyy")}
          </Text>
        </Block>
      </TouchableWithoutFeedback>
    </Block>
  );
};

export default Event;
