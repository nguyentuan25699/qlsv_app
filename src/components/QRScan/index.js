import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  StyleSheet,
  Button,
  Dimensions,
  Alert,
} from "react-native";
import { BarCodeScanner } from "expo-barcode-scanner";
import { useDispatch } from "react-redux";
import AnimatedLoader from "react-native-animated-loader";
import eventJoinActions from "../../Redux/Actions/eventJoinActions";
import { pushNotification } from "../../Service/axiosClient";
const { width } = Dimensions.get("screen");

export default function QRScan(props) {
  //!Const
  const dispatch = useDispatch();
  //!State
  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setscanned] = useState(false);
  const [loading, setloading] = useState(false);

  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === "granted");
    })();
  }, []);

  const handleBarCodeScanned = ({ type, data }) => {
    setscanned(true);
    // setloading(true);
    const idEvent = data.split(",")[1];
    if (idEvent === props.idEvent) {
      Alert.alert("Thông báo!", `Sự kiện không hợp lệ!`, [
        // {
        //   text: "Cancel",
        //   onPress: () => setscanned(false),
        //   style: "cancel",
        // },
        { text: "OK", onPress: () => setscanned(false), style: "ok" },
      ]);
    } else {
      Alert.alert(
        "Thông báo!",
        `Điểm danh sinh viên: ${data.split(",")[2]} với sự kiện: ${
          data.split(",")[0]
        } ?`,
        [
          {
            text: "Cancel",
            onPress: () => setscanned(false),
            style: "cancel",
          },
          {
            text: "OK",
            onPress: () => {
              setscanned(false);
              setloading(true);
              dispatch(
                eventJoinActions.joinEvent(
                  {
                    userId: data.split(",")[3],
                    eventId: data.split(",")[1],
                  },
                  {
                    success: () => {
                      const dataNotifi = {
                        content:
                          "Bạn đã tham gia sự kiện " +
                          data.split(",")[0] +
                          " thành công!",
                        time: new Date().toISOString(),
                        type: "user",
                        eventId: data.split(",")[1],
                        userId: data.split(",")[3],
                      };
                      dispatch(
                        notificationActions.createNotification(dataNotifi)
                      );
                      Alert.alert(
                        "Thông báo!",
                        `Điểm danh sinh viên: ${
                          data.split(",")[2]
                        } với sự kiện: ${data.split(",")[0]} thành công!`
                      );
                      pushNotification(
                        data.split(",")[4],
                        "Thông báo!",
                        `Bạn đã tham gia sự kiện ${
                          data.split(",")[2]
                        } thành công!`
                      );
                      setloading(false);
                    },
                    failed: (mess) => {
                      Alert.alert(
                        "Thông báo!",
                        `Điểm danh sinh viên: ${
                          data.split(",")[2]
                        } với sự kiện: ${
                          data.split(",")[0]
                        } thất bại! Lỗi: ${mess}`
                      );
                      setloading(false);
                    },
                  }
                )
              );
            },
            style: "ok",
          },
        ]
      );
    }
  };

  if (hasPermission === null) {
    return <Text>Requesting for camera permission</Text>;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

  return (
    <View style={styles.container}>
      <AnimatedLoader
        visible={loading}
        overlayColor="rgba(255,255,255,0.75)"
        source={require("../../9764-loader.json")}
        animationStyle={{ width: 100, height: 100 }}
        speed={1}
      >
        <Text style={{ fontSize: 18, fontWeight: "bold" }}>Xin hãy đợi...</Text>
      </AnimatedLoader>
      <BarCodeScanner
        onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
        style={StyleSheet.absoluteFillObject}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  //   scan: { width: width / 1.1, height: width / 1.1 },
  container: {
    backgroundColor: "black",
    width: width / 1.35,
    height: width / 1.35,
    alignSelf: "center",
    justifyContent: "center",
    alignContent: "center",
  },
});
