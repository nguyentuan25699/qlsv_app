import { Block, Text } from "galio-framework";
import React from "react";
import { TouchableWithoutFeedback } from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { Icon } from "../../../components";
import styles from "./styles";
import moment from "moment";

const CustomNotifications = (props) => {
  const {
    notification,
    horizontal,
    onPress,
    full,
    numberOfLines,
    style,
    timeColor,
    imageStyle,
  } = props;
  const imageStyles = [
    styles.image,
    full ? styles.fullImage : styles.horizontalImage,
    imageStyle,
  ];
  return (
    <Block
      row
      card
      flex
      style={[styles.notification, styles.shadow, style]}
      // key={key}
    >
      <TouchableWithoutFeedback onPress={onPress}>
        <Block style={styles.notificationDescription}>
          <Icon
            name="notifications-active"
            family="Material-Icons"
            size={25}
            // style={{ alignSeft: "center" }}
          />
        </Block>
      </TouchableWithoutFeedback>
      <TouchableWithoutFeedback onPress={onPress}>
        <Block flex style={styles.notificationDescription}>
          <Text
            size={14}
            style={styles.notificationTitle}
            numberOfLines={numberOfLines}
          >
            {notification.content}
          </Text>
          <Text
            size={12}
            muted={!timeColor}
            color={timeColor}
            numberOfLines={1}
          >
            <MaterialCommunityIcons
              name="clock-outline"
              color={timeColor}
              size={12}
            />{" "}
            {moment(notification.time).format("HH:mm DD-MM-yyyy")}
          </Text>
        </Block>
      </TouchableWithoutFeedback>
    </Block>
  );
};

export default CustomNotifications;
