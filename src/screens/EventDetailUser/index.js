import { useNavigation, useRoute } from "@react-navigation/core";
import { LinearGradient } from "expo-linear-gradient";
import { Block, Button, Text, theme } from "galio-framework";
import moment from "moment";
import queryString from "query-string";
import React, { useState } from "react";
import { Alert, Dimensions, ImageBackground, ScrollView } from "react-native";
import AnimatedLoader from "react-native-animated-loader";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import QRCode from "react-native-qrcode-svg";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { useDispatch, useSelector } from "react-redux";
import { Icon } from "../../../components";
import eventJoinActions from "../../Redux/Actions/eventJoinActions";
import { BASE_URL_IMAGE } from "../../Service/ServiceURL";
import styles from "./styles";
const { width } = Dimensions.get("screen");
import * as Notifications from "expo-notifications";
import { pushNotification } from "../../Service/axiosClient";
import notificationActions from "../../Redux/Actions/notificationActions";

const EventDetailUser = () => {
  //!Const
  const idUser = useSelector((state) => state.userReducer.data.user.id);
  const userName = useSelector((state) => state.userReducer.data.user.name);
  const route = useRoute();
  const Navigation = useNavigation();
  const listEventJoin = useSelector(
    (state) => state.eventJoinReducer.listEventJoin
  );
  const dispatch = useDispatch();
  //!State
  const [eventDetail, seteventDetail] = useState({});
  const [registed, setregisted] = useState(false);
  const [loading, setloading] = React.useState(false);
  const [disabled, setdisabled] = React.useState(false);
  const [idEventJoin, setidEventJoin] = React.useState("");
  const [tokenDivice, settokenDivice] = React.useState("");
  //!useEffect
  React.useEffect(() => {
    seteventDetail(route.params);
  }, [route.params]);
  React.useEffect(() => {
    dispatch(
      eventJoinActions.getListEventJoin(
        queryString.stringify({
          userId: idUser,
          populate: "eventId",
        })
      )
    );
  }, []);
  React.useEffect(() => {
    listEventJoin.map((index) => {
      if (
        !!index.eventId.id &&
        index.status === "Đã Đăng ký" &&
        index.eventId.status !== "Đã diễn ra" &&
        index.eventId.id === route.params.id
      ) {
        setidEventJoin(index.id);
        setregisted(true);
      }
    });
  }, [listEventJoin]);
  React.useEffect(() => {
    handleGetTokenNotifications().then((token) => {
      settokenDivice(token);
    });
  }, []);
  //!Function
  const handleGetTokenNotifications = async () => {
    const { status } = await Notifications.getPermissionsAsync();
    if (status !== "granted") {
      const { status } = await Notifications.requestPermissionsAsync();
      if (status !== "grandted") {
        Alert.alert("No permission to show notifications!");
        return;
      }
    }
    const tokenData = await Notifications.getExpoPushTokenAsync();
    const token = tokenData.data;
    console.log({ token });
    if (Platform.OS === "android") {
      Notifications.setNotificationChannelAsync("default", {
        name: "default",
        importance: Notifications.AndroidImportance.MAX,
        priority: "max",
        vibrationPattern: [0, 250, 250, 250],
        lightColor: "#FF231F7C",
      });
    }
    return token;
  };
  const handleRegister = () => {
    setloading(true);
    dispatch(
      eventJoinActions.registerEvent(
        { eventId: route.params.id },
        {
          success: (data) => {
            handleGetTokenNotifications()
              .then((token) => {
                pushNotification(
                  token,
                  "Thông báo!",
                  `Đã đăng ký sự kiện ${route.params.name} thành công!`
                );
              })
              .catch((err) => {
                Alert.alert("ERROR", err);
              });
            Alert.alert("Thông báo!", "Đăng ký thành công!");
            setloading(false);
            setregisted(true);
            setidEventJoin(data.id);
            dispatch(
              eventJoinActions.getListEventJoin(
                queryString.stringify({
                  userId: idUser,
                  populate: "eventId",
                })
              )
            );
            const dataNotifi = {
              content: "Bạn đã đăng ký sự kiện " + route.params.name + "!",
              time: new Date().toISOString(),
              type: "user",
              eventId: eventDetail.id,
              userId: idUser,
            };
            dispatch(notificationActions.createNotification(dataNotifi));
          },
          failed: (mess) => {
            Alert.alert("Thông báo!", "Đăng ký thất bại! Lỗi: " + mess + "!");
            setloading(false);
          },
        }
      )
    );
  };
  const handleUnRegister = () => {
    setloading(true);
    dispatch(
      eventJoinActions.deleteEventJoin(idEventJoin, {
        success: () => {
          handleGetTokenNotifications()
            .then((token) => {
              pushNotification(
                token,
                "Thông báo!",
                `Huỷ đăng ký sự kiện ${route.params.name} thành công!`
              );
            })
            .catch((err) => {
              Alert.alert("ERROR", err);
            });
          setloading(false);
          setregisted(false);
          setidEventJoin("");
          Alert.alert("Thông báo!", "Huỷ đăng ký thành công!");
          const dataNotifi = {
            content: "Bạn đã huỷ đăng ký sự kiện " + route.params.name + "!",
            time: new Date().toISOString(),
            type: "user",
            eventId: eventDetail.id,
            userId: idUser,
          };
          dispatch(notificationActions.createNotification(dataNotifi));
          dispatch(
            eventJoinActions.getListEventJoin(
              queryString.stringify({
                userId: idUser,
                populate: "eventId",
              })
            )
          );
        },
        failed: (mess) => {
          Alert.alert("Thông báo!", "Huỷ đăng ký thất bại! Lỗi: " + mess + "!");
          setloading(false);
        },
      })
    );
  };
  console.log({ tokenDivice });
  //!Render
  return (
    <Block flex style={styles.event}>
      <AnimatedLoader
        visible={loading}
        overlayColor="rgba(255,255,255,0.75)"
        source={require("../../9764-loader.json")}
        animationStyle={{ width: 100, height: 100 }}
        speed={1}
      >
        <Text style={{ fontSize: 18, fontWeight: "bold" }}>Xin hãy đợi...</Text>
      </AnimatedLoader>
      <ImageBackground
        source={{ uri: BASE_URL_IMAGE + eventDetail.image }}
        style={styles.eventContainer}
        imageStyle={styles.eventImage}
        resizeMode="cover"
      >
        <Block flex style={styles.eventDetails}>
          <LinearGradient
            colors={["rgba(0,0,0,1)", "rgba(0,0,0,0)"]}
            style={styles.gradient}
          >
            <TouchableWithoutFeedback
              disabled={disabled}
              style={{
                justifyContent: "center",
                alignItems: "center",
                width: theme.SIZES.BASE * 4,
                height: theme.SIZES.BASE * 4,
              }}
              onPress={() => {
                Navigation.goBack();
                setdisabled(true);
              }}
            >
              <Icon
                name="arrow-back-ios"
                family="Material-Icons"
                color="white"
                size={20}
                style={{
                  alignSelf: "center",
                }}
              />
            </TouchableWithoutFeedback>
          </LinearGradient>
        </Block>
      </ImageBackground>
      <Block flex={0.7}>
        <Block style={styles.options}>
          <ScrollView vertical={true} showsVerticalScrollIndicator={false}>
            <Block
              row
              space="between"
              style={{ paddingVertical: 16, alignItems: "baseline" }}
            >
              <Text h4 bold muted>
                {eventDetail.name}
              </Text>
            </Block>
            {registed ? (
              <Block style={{ padding: theme.SIZES.BASE }}>
                <Block middle>
                  <QRCode
                    id="QRCode"
                    value={
                      eventDetail.name +
                      "," +
                      eventDetail.id +
                      "," +
                      userName +
                      "," +
                      idUser +
                      "," +
                      tokenDivice
                    }
                    size={width / 2}
                    style={{ width: width, height: width }}
                  />

                  <Block>
                    {route.params.status.split(" - ")[0] === "Sắp diễn ra" &&
                    route.params.isOpenRegister &&
                    new Date(route.params.startTimeRegister).getTime() <=
                      new Date().getTime() &&
                    new Date(route.params.endTimeRegister).getTime() >=
                      new Date().getTime() ? (
                      <Button
                        shadowless
                        color="danger"
                        style={[styles.button, styles.shadow]}
                        onPress={handleUnRegister}
                      >
                        HUỶ ĐĂNG KÍ SỰ KIỆN
                      </Button>
                    ) : null}
                  </Block>
                </Block>
              </Block>
            ) : (
              <>
                <Block space="around" style={{ flexWrap: "wrap" }}>
                  <Block row style={styles.detail}>
                    <MaterialCommunityIcons
                      name="information-outline"
                      color={theme.COLORS.MUTED}
                      size={16}
                    />
                    <Text
                      muted
                      size={16}
                      style={{ paddingHorizontal: theme.SIZES.BASE / 2 }}
                    >
                      Trạng thái: {eventDetail.status}
                    </Text>
                  </Block>
                  <Block row style={styles.detail}>
                    <Icon
                      name="description"
                      family="Material-Icons"
                      color={theme.COLORS.MUTED}
                      size={16}
                    />
                    <Text
                      muted
                      size={16}
                      style={{ paddingHorizontal: theme.SIZES.BASE / 2 }}
                    >
                      Mô tả: {eventDetail.description}
                    </Text>
                  </Block>
                  <Block row style={styles.detail}>
                    <Icon
                      name="lock-clock"
                      family="Material-Icons"
                      color={theme.COLORS.MUTED}
                      size={16}
                    />
                    <Text
                      muted
                      size={16}
                      style={{ paddingHorizontal: theme.SIZES.BASE / 2 }}
                    >
                      Thời gian đăng ký:{" "}
                      {moment(eventDetail.startTimeRegister).format(
                        "HH:mm DD-MM-yyy"
                      )}{" "}
                      -{" "}
                      {moment(eventDetail.endTimeRegister).format(
                        "HH:mm DD-MM-yyy"
                      )}
                    </Text>
                  </Block>
                  <Block row style={styles.detail}>
                    <Icon
                      name="clock"
                      family="font-awesome-5"
                      color={theme.COLORS.MUTED}
                      size={16}
                    />
                    <Text
                      muted
                      size={16}
                      style={{ paddingHorizontal: theme.SIZES.BASE / 2 }}
                    >
                      Thời gian:{" "}
                      {moment(eventDetail.startTime).format("HH:mm DD-MM-yyy")}{" "}
                      - {moment(eventDetail.endTime).format("HH:mm DD-MM-yyy")}
                    </Text>
                  </Block>
                  <Block row style={styles.detail}>
                    <Icon
                      name="map-marked-alt"
                      family="font-awesome-5"
                      color={theme.COLORS.MUTED}
                      size={16}
                    />
                    <Text
                      muted
                      size={16}
                      style={{ paddingHorizontal: theme.SIZES.BASE / 2 }}
                    >
                      Địa điểm :{eventDetail.address}
                    </Text>
                  </Block>
                </Block>
                <Block style={{ padding: theme.SIZES.BASE }}>
                  <Block>
                    {route.params.status === "Sắp diễn ra" &&
                    route.params.isOpenRegister &&
                    new Date(route.params.startTimeRegister).getTime() <=
                      new Date().getTime() &&
                    new Date(route.params.endTimeRegister).getTime() >=
                      new Date().getTime() ? (
                      <Button
                        shadowless
                        color="info"
                        style={[styles.button, styles.shadow]}
                        onPress={handleRegister}
                      >
                        ĐĂNG KÍ THAM GIA SỰ KIỆN
                      </Button>
                    ) : null}
                  </Block>
                </Block>
              </>
            )}
          </ScrollView>
        </Block>
      </Block>
    </Block>
  );
};

export default EventDetailUser;
