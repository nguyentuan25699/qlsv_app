import { useNavigation, useRoute } from "@react-navigation/core";
import axios from "axios";
import { LinearGradient } from "expo-linear-gradient";
import { Formik } from "formik";
import { Block, Button, Input, Text, theme } from "galio-framework";
import moment from "moment";
import React from "react";
import { Alert, Keyboard, KeyboardAvoidingView, LogBox } from "react-native";
import {
  ScrollView,
  TouchableWithoutFeedback,
} from "react-native-gesture-handler";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import ModalDropdown from "react-native-modal-dropdown";
import { useDispatch, useSelector } from "react-redux";
import * as yup from "yup";
import { Icon } from "../../../components";
import { materialTheme } from "../../../constants";
import ImageUpload from "../../components/ImageUpload";
import uploadActions from "../../Redux/Actions/uploadActions";
import userActions from "../../Redux/Actions/userActions";
import ServiceURL, { BASE_URL, BASE_URL_IMAGE } from "../../Service/ServiceURL";
import styles from "./styles";

LogBox.ignoreAllLogs(); //Ignore all log notifications

const UpdateDetailUser = (props) => {
  //!State
  const route = useRoute();
  const Navigation = useNavigation();
  const [userDetail, setuserDetail] = React.useState({
    address: "",
    gender: "",
    phoneNumber: "",
    // team: "",
    // image: "",
    dateOfBird: "",
    dateOfBirthActive: false,
    homeTownActive: false,
    phoneNumberActive: false,
  });
  const [isDatePickerVisible, setisDatePickerVisible] = React.useState(false);
  const [singleFile, setSingleFile] = React.useState(null);
  //!Const
  const token = useSelector(
    (state) => state.userReducer.data.tokens.access.token
  );
  const dispatch = useDispatch();
  const vnf_regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
  const userSchema = yup.object().shape({
    dateOfBird: yup.string().required("Vui lòng nhập ngày sinh!"),
    // password: yup
    //   .string()
    //   .required("Vui lòng nhập mật khẩu!")
    //   .min(8, ({ min }) => "Mật khẩu phải có ít nhất 8 ký tự!"),
    gender: yup.string().required("Vui lòng chọn giới tính!"),
    address: yup.string().required("Vui lòng chọn quê quán!"),
    phoneNumber: yup
      .string()
      .required("Vui lòng nhập số điện thoại!")
      .matches(vnf_regex, "Số điện thoại không đúng định dạng!"),
  });
  //!UseEffect
  React.useEffect(() => {
    setuserDetail({
      ...userDetail,
      address: route.params.address,
      gender: route.params.gender,
      phoneNumber: route.params.phoneNumber,
      // team: route.params.team,
      image: route.params.image,
      dateOfBird: route.params.dateOfBird,
    });
  }, []);
  //!Function
  const submitForm = (value) => {
    const detailUpdate = {
      name: route.params.name,
      dateOfBird: value.dateOfBird,
      address: value.address,
      gender: value.gender,
      class: route.params.class,
      schoolYear: route.params.schoolYear,
      faculty: route.params.faculty,
      phoneNumber: value.phoneNumber,
      studentCode: route.params.studentCode,
      role: route.params.role,
    };
    dispatch(
      userActions.editUsers(detailUpdate, route.params.id, {
        success: () => {
          Alert.alert("Thông báo!", "Cập nhật thông tin thành công!");
          Navigation.goBack();
        },
        failed: (mess) => {
          Alert.alert(
            "Thông báo!",
            "Cập nhật thông tin thất bại! Lỗi : " + mess + "!"
          );
        },
      })
    );
  };
  return (
    <LinearGradient
      start={{ x: 0, y: 0 }}
      end={{ x: 0.25, y: 1.1 }}
      locations={[0.2, 1]}
      // colors={["#7986CB", "#1A237E"]}
      colors={["#7986CB", "#1A237E"]}
      style={[styles.signin, { flex: 1, paddingTop: theme.SIZES.BASE * 4 }]}
    >
      <Block flex middle style={{ flex: 1 }}>
        <KeyboardAvoidingView
          behavior="height"
          enabled
          style={{
            flex: 1,
            alignContent: "center",
            justifyContent: "center",
          }}
        >
          <ScrollView
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            style={{
              flex: 1,
              // backgroundColor: "black",
              // alignContent: "center",
              paddingTop: theme.SIZES.BASE * 5,
            }}
            // contentContainerStyle={{ justifyContent: "flex-end" }}
          >
            <TouchableWithoutFeedback
              style={{
                justifyContent: "flex-start",
                alignItems: "center",
                // backgroundColor: "black",
                width: theme.SIZES.BASE * 4,
                height: theme.SIZES.BASE * 4,
              }}
              onPress={() => {
                Navigation.goBack();
              }}
            >
              <Icon
                name="arrow-back-ios"
                family="Material-Icons"
                color="white"
                size={20}
                style={{
                  alignSelf: "center",
                }}
              />
            </TouchableWithoutFeedback>
            <Block
              block
              bottom
              space="between"
              style={
                {
                  // marginVertical: theme.SIZES.BASE * 1.875,
                  // justifyContent: "flex-end",
                }
              }
            >
              {/* <Block
                block
                center
                space="between"
                style={{
                  marginVertical: theme.SIZES.BASE * 1.875,
                  flex: 2,
                  justifyContent: "center",
                  //
                }}
              > */}
              {/* <Block center>
                  <Image
                    source={require("../../img/Logo.png")}
                    style={{
                      height: theme.SIZES.BASE * 10,
                      width: theme.SIZES.BASE * 10,
                      marginTop: -theme.SIZES.BASE,
                      opacity: 0.7,
                      borderRadius: 3,
                    }}
                  />
                </Block> */}
              {/* </Block> */}
              <Formik
                validationSchema={userSchema}
                enableReinitialize
                initialValues={userDetail}
                onSubmit={submitForm}
              >
                {({ values, setFieldValue, handleSubmit, errors, touched }) => {
                  return (
                    <Block block style={{ flex: 1 }}>
                      <Block
                        center
                        style={{
                          justifyContent: "center",
                          marginTop: 20,
                        }}
                      >
                        <Block>
                          <ImageUpload
                            avatar={BASE_URL_IMAGE + values.image}
                            sendReult={async (result) => {
                              if (!!result && result !== null) {
                                // ImagePicker saves the taken photo to disk and returns a local URI to it
                                let localUri = result.uri;
                                let filename = localUri.split("/").pop();
                                console.log({ filename });
                                // Infer the type of the image
                                let match = /\.(\w+)$/.exec(filename);
                                let type = match
                                  ? `image/${match[1]}`
                                  : `image`;
                                // Upload the image using the fetch and FormData APIs
                                let formData = new FormData();
                                // Assume "photo" is the name of the form field the server expects
                                formData.append(
                                  "image",
                                  result.base64,
                                  filename
                                );
                                // console.log({ formData });
                                // const config = {
                                //   headers: {
                                //     Authorization: `Bearer ${token}`,
                                //   },
                                // };

                                // const bodyParameters = {
                                //   formData,
                                // };
                                // axios
                                //   .post(
                                //     BASE_URL + ServiceURL.Upload,
                                //     bodyParameters,
                                //     config
                                //   )
                                //   .then((res) => {
                                //     console.log("response", res);
                                //   })
                                //   .catch((err) => {
                                //     console.log("error", err);
                                //   });
                                dispatch(
                                  uploadActions.uploadImage(formData, {
                                    success: (url) => {
                                      console.log({ url });
                                    },
                                    failed: (mess) => {
                                      console.log({ mess });
                                    },
                                  })
                                );
                              }
                            }}
                          />
                          <DateTimePickerModal
                            maximumDate={new Date()}
                            isVisible={isDatePickerVisible}
                            mode="date"
                            display={"spinner"}
                            onConfirm={(date) => {
                              setFieldValue("dateOfBird", date.toISOString());
                            }}
                            onCancel={() => {
                              setisDatePickerVisible(false);
                            }}
                            onHide={() => {
                              setisDatePickerVisible(false);
                            }}
                          />
                          <TouchableWithoutFeedback
                            onPress={() => {
                              setFieldValue(
                                "dateOfBirthActive",
                                !values.dateOfBirthActive
                              );
                              setisDatePickerVisible(true);
                            }}
                          >
                            <Input
                              borderless
                              showSoftInputOnFocus={false}
                              color="white"
                              placeholder="Ngày sinh"
                              type="numeric"
                              value={moment(values.dateOfBird).format(
                                "DD/MM/YYYY"
                              )}
                              autoCapitalize="none"
                              bgColor="transparent"
                              onBlur={() => {
                                setisDatePickerVisible(false);
                                // setuserDetail({
                                //   userDetail,
                                //   studentCodeActive: !clone.studentCodeActive,
                                // });
                                setFieldValue(
                                  "dateOfBirthActive",
                                  !values.dateOfBirthActive
                                );
                              }}
                              onFocus={() => {
                                setisDatePickerVisible(true);
                                Keyboard.dismiss;
                                setFieldValue(
                                  "dateOfBirthActive",
                                  !values.dateOfBirthActive
                                );
                              }}
                              placeholderTextColor={
                                materialTheme.COLORS.PLACEHOLDER
                              }
                              //   onChangeText={(text) => {
                              //     setFieldValue("studentCode", text);
                              //   }}
                              style={[
                                styles.input,
                                values.dateOfBirthActive
                                  ? styles.inputActive
                                  : null,
                              ]}
                            />
                          </TouchableWithoutFeedback>
                          {errors.dateOfBird && touched.dateOfBird && (
                            <Text style={{ fontSize: 12, color: "red" }}>
                              {errors.dateOfBird}
                            </Text>
                          )}
                          <Block style={{ marginTop: 8 }}>
                            <ModalDropdown
                              style={[styles.qty]}
                              onSelect={(index, value) => {
                                setFieldValue("gender", value);
                              }}
                              dropdownStyle={styles.dropdown}
                              dropdownTextStyle={{
                                paddingLeft: theme.SIZES.BASE,
                                fontSize: 12,
                              }}
                              options={["Nam", "Nữ"]}
                            >
                              <Text
                                style={[
                                  values.gender !== ""
                                    ? { color: "white" }
                                    : {
                                        color: materialTheme.COLORS.PLACEHOLDER,
                                      },
                                ]}
                              >
                                {values.gender === ""
                                  ? "Giới tính"
                                  : values.gender}
                              </Text>
                            </ModalDropdown>
                            {errors.gender && touched.gender && (
                              <Text style={{ fontSize: 12, color: "red" }}>
                                {errors.gender}
                              </Text>
                            )}
                          </Block>
                          <Input
                            // viewPass
                            value={values.phoneNumber}
                            borderless
                            type="numeric"
                            color="white"
                            iconColor="white"
                            placeholder="Số điện thoại"
                            bgColor="transparent"
                            onBlur={() => {
                              setFieldValue("phoneActive", !values.phoneActive);
                            }}
                            onFocus={() => {
                              setFieldValue("phoneActive", !values.phoneActive);
                            }}
                            placeholderTextColor={
                              materialTheme.COLORS.PLACEHOLDER
                            }
                            onChangeText={(text) => {
                              // setuserDetail({ userDetail, password: text });
                              setFieldValue("phoneNumber", text);
                            }}
                            style={[
                              styles.input,
                              values.phoneActive ? styles.inputActive : null,
                            ]}
                          />
                          {errors.phoneNumber && touched.phoneNumber && (
                            <Text style={{ fontSize: 12, color: "red" }}>
                              {errors.phoneNumber}
                            </Text>
                          )}
                          <Input
                            // viewPass
                            value={values.address}
                            borderless
                            type="default"
                            color="white"
                            iconColor="white"
                            placeholder="Địa chỉ"
                            bgColor="transparent"
                            onBlur={() => {
                              setFieldValue(
                                "homeTownActive",
                                !values.homeTownActive
                              );
                            }}
                            onFocus={() => {
                              setFieldValue(
                                "homeTownActive",
                                !values.homeTownActive
                              );
                            }}
                            placeholderTextColor={
                              materialTheme.COLORS.PLACEHOLDER
                            }
                            onChangeText={(text) => {
                              // setuserDetail({ userDetail, password: text });
                              setFieldValue("address", text);
                            }}
                            style={[
                              styles.input,
                              values.homeTownActive ? styles.inputActive : null,
                            ]}
                          />
                          {errors.address && touched.address && (
                            <Text style={{ fontSize: 12, color: "red" }}>
                              {errors.address}
                            </Text>
                          )}
                        </Block>
                        {/* <Text
                          color={theme.COLORS.WHITE}
                          size={theme.SIZES.FONT * 0.75}
                          onPress={() => {
                            // Navigation.navigate("ForgotPass"),
                            // Navigation.canGoBack();
                          }}
                          style={{
                            alignSelf: "flex-end",
                            lineHeight: theme.SIZES.FONT * 2,
                          }}
                        >
                          Quên mật khẩu?
                        </Text> */}
                      </Block>
                      <Block center>
                        <Block center flex style={{ marginTop: 20 }}>
                          <Button
                            size="large"
                            shadowless
                            color={materialTheme.COLORS.MICHAEL_COLOR}
                            style={{ height: 48 }}
                            onPress={handleSubmit}
                          >
                            CẬP NHẬT THÔNG TIN
                          </Button>

                          {/* <Button
                            size="large"
                            color="transparent"
                            shadowless
                            onPress={() => {
                              // Navigation.navigate("Resgiter");
                            }}
                          >
                            <Text
                              center
                              color={theme.COLORS.WHITE}
                              size={theme.SIZES.FONT * 0.75}
                              style={{ marginTop: 20 }}
                            >
                              {"Chưa có tài khoản? Đăng ký"}
                            </Text>
                          </Button> */}
                        </Block>
                      </Block>
                    </Block>
                  );
                }}
              </Formik>
            </Block>
          </ScrollView>
        </KeyboardAvoidingView>
      </Block>
    </LinearGradient>
  );
};

export default UpdateDetailUser;
