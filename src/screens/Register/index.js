import { LinearGradient } from "expo-linear-gradient";
import { Block, Button, Input, Text, theme } from "galio-framework";
import React from "react";
import { Alert, Image, KeyboardAvoidingView } from "react-native";
import { materialTheme } from "../../../constants";
import styles from "./styles";
import { ScrollView } from "react-native-gesture-handler";
import { Formik } from "formik";
import * as yup from "yup";
import { useNavigation } from "@react-navigation/core";

const Register = () => {
  const [register, setregister] = React.useState({
    studentCode: "",
    name: "",
    password: "",
    email: "",
    emailActive: false,
    nameActive: false,
    studentCodeActive: false,
    passwordActive: false,
  });
  const Navigation = useNavigation();
  const resSchema = yup.object().shape({
    studentCode: yup
      .string()
      .min(10, ({ min }) => "Mã sinh viên phải có 10 kí tự")
      .max(10, ({ max }) => "Mã sinh viên phải có 10 kí tự")
      .required("Vui lòng nhập mã sinh viên!"),
    password: yup
      .string()
      .required("Vui lòng nhập mật khẩu!")
      .min(8, ({ min }) => "Mật khẩu phải có ít nhất 8 ký tự!"),
    email: yup
      .string()
      .required("Vui lòng nhập Email!")
      .email("Email không đúng định dạng!"),
    name: yup.string().required("Vui lòng nhập tên!"),
  });

  return (
    <LinearGradient
      start={{ x: 0, y: 0 }}
      end={{ x: 0.25, y: 1.1 }}
      locations={[0.2, 1]}
      colors={["#7986CB", "#1A237E"]}
      style={[styles.resgiter, { flex: 1, paddingTop: theme.SIZES.BASE * 4 }]}
    >
      <Block flex middle style={{ flex: 1 }}>
        <KeyboardAvoidingView
          behavior="height"
          enabled
          style={{ flex: 1, alignContent: "center", justifyContent: "center" }}
        >
          <ScrollView
            style={{
              flex: 1,

              // alignContent: "center",
              paddingTop: theme.SIZES.BASE * 5,
            }}
            // contentContainerStyle={{ justifyContent: "flex-end" }}
          >
            <Block
              block
              bottom
              space="between"
              style={{
                marginVertical: theme.SIZES.BASE * 1.875,

                // justifyContent: "flex-end",
              }}
            >
              <Block
                block
                center
                space="between"
                style={{
                  marginVertical: theme.SIZES.BASE * 1.875,
                  flex: 2,
                  justifyContent: "center",
                  //
                }}
              >
                <Block center>
                  <Image
                    source={require("../../img/Logo.png")}
                    style={{
                      height: theme.SIZES.BASE * 10,
                      width: theme.SIZES.BASE * 10,
                      marginTop: -theme.SIZES.BASE * 2,
                      opacity: 0.7,
                      borderRadius: 3,
                    }}
                  />
                </Block>
              </Block>
              <Formik
                enableReinitialize
                initialValues={register}
                validationSchema={resSchema}
                onSubmit={(value) => {
                  Alert.alert(
                    "Sign in action",
                    `studentCode: ${value.studentCode} email: ${value.email} Password: ${value.password}`
                  );
                }}
              >
                {({ values, setFieldValue, handleSubmit, errors, touched }) => {
                  return (
                    <Block block style={{ flex: 1 }}>
                      <Block
                        center
                        style={{
                          justifyContent: "center",
                          marginTop: 20,
                        }}
                      >
                        <Block>
                          <Input
                            borderless
                            color="white"
                            placeholder="Mã sinh viên..."
                            type="numeric"
                            value={values.studentCode}
                            autoCapitalize="none"
                            bgColor="transparent"
                            onBlur={() => {
                              setFieldValue(
                                "studentCodeActive",
                                !values.studentCodeActive
                              );
                            }}
                            onFocus={() => {
                              setFieldValue(
                                "studentCodeActive",
                                !values.studentCodeActive
                              );
                            }}
                            placeholderTextColor={
                              materialTheme.COLORS.PLACEHOLDER
                            }
                            onChangeText={(text) => {
                              setFieldValue("studentCode", text);
                            }}
                            style={[
                              styles.input,
                              values.studentCodeActive
                                ? styles.inputActive
                                : null,
                            ]}
                          />
                          {errors.studentCode && touched.studentCode && (
                            <Text style={{ fontSize: 12, color: "red" }}>
                              {errors.studentCode}
                            </Text>
                          )}
                          <Input
                            borderless
                            color="white"
                            placeholder="Họ tên..."
                            type="default"
                            value={values.name}
                            autoCapitalize="none"
                            bgColor="transparent"
                            onBlur={() => {
                              setFieldValue("nameActive", !values.nameActive);
                            }}
                            onFocus={() => {
                              setFieldValue("nameActive", !values.nameActive);
                            }}
                            placeholderTextColor={
                              materialTheme.COLORS.PLACEHOLDER
                            }
                            onChangeText={(text) => {
                              setFieldValue("name", text);
                            }}
                            style={[
                              styles.input,
                              values.nameActive ? styles.inputActive : null,
                            ]}
                          />
                          {errors.name && touched.name && (
                            <Text style={{ fontSize: 12, color: "red" }}>
                              {errors.name}
                            </Text>
                          )}
                          <Input
                            borderless
                            color="white"
                            placeholder="Email..."
                            type="email-address"
                            value={values.email}
                            autoCapitalize="none"
                            bgColor="transparent"
                            onBlur={() => {
                              setFieldValue("emailActive", !values.emailActive);
                            }}
                            onFocus={() => {
                              setFieldValue("emailActive", !values.emailActive);
                            }}
                            placeholderTextColor={
                              materialTheme.COLORS.PLACEHOLDER
                            }
                            onChangeText={(text) => {
                              setFieldValue("email", text);
                            }}
                            style={[
                              styles.input,
                              values.emailActive ? styles.inputActive : null,
                            ]}
                          />
                          {errors.email && touched.email && (
                            <Text style={{ fontSize: 12, color: "red" }}>
                              {errors.email}
                            </Text>
                          )}
                          <Input
                            password
                            viewPass
                            // label="Nugyeenx"
                            value={values.password}
                            borderless
                            color="white"
                            iconColor="white"
                            placeholder="Mật khẩu..."
                            bgColor="transparent"
                            onBlur={() => {
                              setFieldValue(
                                "passwordActive",
                                !values.passwordActive
                              );
                            }}
                            onFocus={() => {
                              setFieldValue(
                                "passwordActive",
                                !values.passwordActive
                              );
                            }}
                            placeholderTextColor={
                              materialTheme.COLORS.PLACEHOLDER
                            }
                            onChangeText={(text) => {
                              setFieldValue("password", text);
                            }}
                            style={[
                              styles.input,
                              values.passwordActive ? styles.inputActive : null,
                            ]}
                          />
                          {errors.password && touched.password && (
                            <Text style={{ fontSize: 12, color: "red" }}>
                              {errors.password}
                            </Text>
                          )}
                        </Block>

                        {/* <Text
                          color={theme.COLORS.WHITE}
                          size={theme.SIZES.FONT * 0.75}
                          onPress={() => Alert.alert("Not implemented")}
                          style={{
                            alignSelf: "flex-end",
                            lineHeight: theme.SIZES.FONT * 2,
                          }}
                        >
                          Quên mật khẩu?
                        </Text> */}
                      </Block>
                      <Block center>
                        <Block center flex style={{ marginTop: 20 }}>
                          <Button
                            size="large"
                            shadowless
                            color={materialTheme.COLORS.MICHAEL_COLOR}
                            style={{ height: 48 }}
                            onPress={handleSubmit}
                          >
                            ĐĂNG KÝ TÀI KHOẢN
                          </Button>
                          <Button
                            size="large"
                            color="transparent"
                            shadowless
                            onPress={() => {
                              Navigation.navigate("Login");
                            }}
                          >
                            <Text
                              center
                              color={theme.COLORS.WHITE}
                              size={theme.SIZES.FONT * 0.75}
                              style={{ marginTop: 20 }}
                            >
                              {"Đã có tài khoản? Đăng nhập"}
                            </Text>
                          </Button>
                        </Block>
                      </Block>
                    </Block>
                  );
                }}
              </Formik>
            </Block>
          </ScrollView>
        </KeyboardAvoidingView>
      </Block>
    </LinearGradient>
  );
};

export default Register;
