import { theme } from "galio-framework";
import { Dimensions, Platform, StyleSheet } from "react-native";
import { materialTheme } from "../../../constants";
import { HeaderHeight } from "../../../constants/utils";
const { width } = Dimensions.get("window");

export default StyleSheet.create({
  forgot: {
    marginTop: Platform.OS === "android" ? -HeaderHeight : 0,
    alignItems: "center",
    justifyContent: "center",
  },
  input: {
    width: width * 0.9,
    borderRadius: 0,
    borderBottomWidth: 1,
    borderBottomColor: materialTheme.COLORS.PLACEHOLDER,
  },
  inputActive: {
    borderBottomColor: "white",
  },
  imageHorizontal: {
    width: theme.SIZES.BASE * 6.25,
    margin: theme.SIZES.BASE / 2,
  },
});
