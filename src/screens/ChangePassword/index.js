import { useNavigation, useRoute } from "@react-navigation/core";
import { LinearGradient } from "expo-linear-gradient";
import { Formik } from "formik";
import { Block, Button, Input, Text, theme } from "galio-framework";
import React from "react";
import { Alert, Image, KeyboardAvoidingView } from "react-native";
import AnimatedLoader from "react-native-animated-loader";
import {
  ScrollView,
  TouchableWithoutFeedback,
} from "react-native-gesture-handler";
import { useDispatch, useSelector } from "react-redux";
import * as yup from "yup";
import { Icon } from "../../../components";
import { materialTheme } from "../../../constants";
import userActions from "../../Redux/Actions/userActions";
import styles from "./styles";
import queryString from "query-string";

const ChangePassword = () => {
  //!Const
  const route = useRoute();
  const dispatch = useDispatch();
  const Navigation = useNavigation();
  const isChanging = useSelector((state) => state.userReducer.isChanging);
  const token = useSelector(
    (state) => state.userReducer.data.tokens.access.token
  );

  const [changePassword, setchangePassword] = React.useState({
    oldPassword: "",
    newPassword: "",
    newPasswordReType: "",
    passworNowdActive: false,
    newPassworActive: false,
    newPassworReTypeActive: false,
  });
  const passSchema = yup.object().shape({
    oldPassword: yup
      .string()
      .required("Vui lòng nhập mật khẩu hiện tại!")
      .min(8, ({ min }) => `Mật khẩu phải có ít nhất ${min} ký tự!`),
    newPassword: yup
      .string()
      .required("Vui lòng nhập mật khẩu!")
      .matches(
        "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$",
        "Mật khẩu phải có ít nhất 8 kí tự, trong đó chứa ít nhất 1 kí tự và một số!"
      ),
    newPasswordReType: yup
      .string()
      .oneOf([yup.ref("newPassword"), null], "Mật khẩu không trùng khớp!"),
  });
  //!Function
  const handleChangePassword = (value) => {
    const passWord = {
      oldPassword: value.oldPassword,
      newPassword: value.newPassword,
    };
    dispatch(
      userActions.changePassword(
        passWord,
        queryString.stringify({ token: token }),
        {
          success: () => {
            Alert.alert("Thông báo!", "Thay đổi mật khẩu thành công!");
            Navigation.goBack();
          },
          failed: (mess) => {
            Alert.alert(
              "Thông báo!",
              "Thay đổi mật khẩu thất bại! Lỗi: " + mess + "!"
            );
          },
        }
      )
    );
  };
  return (
    <LinearGradient
      start={{ x: 0, y: 0 }}
      end={{ x: 0.25, y: 1.1 }}
      locations={[0.2, 1]}
      // colors={["#7986CB", "#1A237E"]}
      colors={["#7986CB", "#1A237E"]}
      style={[styles.signin, { flex: 1, paddingTop: theme.SIZES.BASE * 4 }]}
    >
      <AnimatedLoader
        visible={isChanging}
        overlayColor="rgba(255,255,255,0.75)"
        source={require("../../9764-loader.json")}
        animationStyle={{ width: 100, height: 100 }}
        speed={1}
      >
        <Text style={{ fontSize: 18, fontWeight: "bold" }}>Xin hãy đợi...</Text>
      </AnimatedLoader>
      <Block flex middle style={{ flex: 1 }}>
        <KeyboardAvoidingView
          behavior="height"
          enabled
          style={{ flex: 1, alignContent: "center", justifyContent: "center" }}
        >
          <ScrollView
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            style={{
              flex: 1,

              // alignContent: "center",
              paddingTop: theme.SIZES.BASE * 5,
            }}
            // contentContainerStyle={{ justifyContent: "flex-end" }}
          >
            <TouchableWithoutFeedback
              style={{
                justifyContent: "flex-start",
                alignItems: "center",
                // backgroundColor: "black",
                width: theme.SIZES.BASE * 4,
                height: theme.SIZES.BASE * 4,
              }}
              onPress={() => {
                Navigation.goBack();
              }}
            >
              <Icon
                name="arrow-back-ios"
                family="Material-Icons"
                color="white"
                size={20}
                style={{
                  alignSelf: "center",
                }}
              />
            </TouchableWithoutFeedback>
            <Block
              block
              bottom
              space="between"
              style={{
                marginVertical: theme.SIZES.BASE * 1.875,

                // justifyContent: "flex-end",
              }}
            >
              <Block
                block
                center
                space="between"
                style={{
                  marginVertical: theme.SIZES.BASE * 1.875,
                  flex: 2,
                  justifyContent: "center",
                  //
                }}
              >
                <Block center>
                  <Image
                    source={require("../../img/Logo.png")}
                    style={{
                      height: theme.SIZES.BASE * 10,
                      width: theme.SIZES.BASE * 10,
                      marginTop: -theme.SIZES.BASE,
                      opacity: 0.7,
                      borderRadius: 3,
                    }}
                  />
                </Block>
              </Block>
              <Formik
                validationSchema={passSchema}
                enableReinitialize
                initialValues={changePassword}
                onSubmit={handleChangePassword}
              >
                {({ values, setFieldValue, handleSubmit, errors, touched }) => {
                  return (
                    <Block block style={{ flex: 1 }}>
                      <Block
                        center
                        style={{
                          justifyContent: "center",
                          marginTop: 20,
                        }}
                      >
                        <Block>
                          <Input
                            password
                            viewPass
                            value={values.oldPassword}
                            borderless
                            color="white"
                            iconColor="white"
                            placeholder="Mật khẩu hiện tại"
                            bgColor="transparent"
                            onBlur={() => {
                              setFieldValue(
                                "oldPasswordActive",
                                !values.oldPasswordActive
                              );
                            }}
                            onFocus={() => {
                              setFieldValue(
                                "oldPasswordActive",
                                !values.oldPasswordActive
                              );
                            }}
                            placeholderTextColor={
                              materialTheme.COLORS.PLACEHOLDER
                            }
                            onChangeText={(text) => {
                              setFieldValue("oldPassword", text);
                            }}
                            style={[
                              styles.input,
                              values.oldPasswordActive
                                ? styles.inputActive
                                : null,
                            ]}
                          />
                          {errors.oldPassword && touched.oldPassword && (
                            <Text style={{ fontSize: 12, color: "red" }}>
                              {errors.oldPassword}
                            </Text>
                          )}
                          <Input
                            password
                            viewPass
                            value={values.newPassword}
                            borderless
                            color="white"
                            iconColor="white"
                            placeholder="Mật khẩu mới"
                            bgColor="transparent"
                            onBlur={() => {
                              setFieldValue(
                                "newPasswordActive",
                                !values.newPasswordActive
                              );
                            }}
                            onFocus={() => {
                              setFieldValue(
                                "newPasswordActive",
                                !values.newPasswordActive
                              );
                            }}
                            placeholderTextColor={
                              materialTheme.COLORS.PLACEHOLDER
                            }
                            onChangeText={(text) => {
                              setFieldValue("newPassword", text);
                            }}
                            style={[
                              styles.input,
                              values.newPasswordActive
                                ? styles.inputActive
                                : null,
                            ]}
                          />
                          {errors.newPassword && touched.newPassword && (
                            <Text style={{ fontSize: 12, color: "red" }}>
                              {errors.newPassword}
                            </Text>
                          )}
                          <Input
                            password
                            viewPass
                            value={values.newPasswordReType}
                            borderless
                            color="white"
                            iconColor="white"
                            placeholder="Nhập lại mật khẩu mới"
                            bgColor="transparent"
                            onBlur={() => {
                              setFieldValue(
                                "newPasswordReTypeActive",
                                !values.newPasswordReTypeActive
                              );
                            }}
                            onFocus={() => {
                              setFieldValue(
                                "newPasswordReTypeActive",
                                !values.newPasswordReTypeActive
                              );
                            }}
                            placeholderTextColor={
                              materialTheme.COLORS.PLACEHOLDER
                            }
                            onChangeText={(text) => {
                              // setchangePassword({ ...changePassword, password: text });
                              setFieldValue("newPasswordReType", text);
                            }}
                            style={[
                              styles.input,
                              values.newPasswordReTypeActive
                                ? styles.inputActive
                                : null,
                            ]}
                          />
                          {errors.newPasswordReType &&
                            touched.newPasswordReType && (
                              <Text style={{ fontSize: 12, color: "red" }}>
                                {errors.newPasswordReType}
                              </Text>
                            )}
                        </Block>
                        {/* 
                        <Text
                          color={theme.COLORS.WHITE}
                          size={theme.SIZES.FONT * 0.75}
                          onPress={() => {
                            // Navigation.navigate("ForgotPass"),
                            // Navigation.canGoBack();
                          }}
                          style={{
                            alignSelf: "flex-end",
                            lineHeight: theme.SIZES.FONT * 2,
                          }}
                        >
                          Quên mật khẩu?
                        </Text> */}
                      </Block>
                      <Block center>
                        <Block center flex style={{ marginTop: 20 }}>
                          <Button
                            size="large"
                            shadowless
                            color={materialTheme.COLORS.MICHAEL_COLOR}
                            style={{ height: 48 }}
                            onPress={handleSubmit}
                          >
                            ĐỔI MẬT KHẨU
                          </Button>
                          {/* <Button
                            size="large"
                            color="transparent"
                            shadowless
                            onPress={() => {
                              // Navigation.navigate("Resgiter");
                            }}
                          >
                            <Text
                              center
                              color={theme.COLORS.WHITE}
                              size={theme.SIZES.FONT * 0.75}
                              style={{ marginTop: 20 }}
                            >
                              {"Chưa có tài khoản? Đăng ký"}
                            </Text>
                          </Button> */}
                        </Block>
                      </Block>
                    </Block>
                  );
                }}
              </Formik>
            </Block>
          </ScrollView>
        </KeyboardAvoidingView>
      </Block>
    </LinearGradient>
  );
};

export default ChangePassword;
