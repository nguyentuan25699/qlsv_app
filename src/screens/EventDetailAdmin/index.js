import { useNavigation, useRoute } from "@react-navigation/core";
import { LinearGradient } from "expo-linear-gradient";
import * as Notifications from "expo-notifications";
import { Block, Button, Text, theme } from "galio-framework";
import moment from "moment";
import React, { useState } from "react";
import { Alert, ImageBackground, ScrollView } from "react-native";
import AnimatedLoader from "react-native-animated-loader";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { Icon } from "../../../components";
import QRScan from "../../components/QRScan";
import { pushNotification } from "../../Service/axiosClient";
import { BASE_URL_IMAGE } from "../../Service/ServiceURL";
import styles from "./styles";

const EventDetailAdmin = () => {
  //!Const
  const route = useRoute();
  const Navigation = useNavigation();
  //!State
  const [eventDetail, seteventDetail] = useState({});
  const [loading, setloading] = React.useState(false);
  const [disabled, setdisabled] = React.useState(false);
  const [scan, setscan] = useState(false);
  //!useEffect
  React.useEffect(() => {
    seteventDetail(route.params);
  }, [route.params]);
  //!Function
  const handleGetTokenNotifications = async () => {
    const { status } = await Notifications.getPermissionsAsync();
    // console.log({ status });
    if (status !== "granted") {
      const { status } = await Notifications.requestPermissionsAsync();
      if (status !== "grandted") {
        Alert.alert("No permission to show notifications!");
        return;
      }
    }
    const tokenData = await Notifications.getExpoPushTokenAsync();
    const token = tokenData.data;
    // console.log({ token });
    if (Platform.OS === "android") {
      Notifications.setNotificationChannelAsync("default", {
        name: "default",
        importance: Notifications.AndroidImportance.MAX,
        priority: "max",
        vibrationPattern: [0, 250, 250, 250],
        lightColor: "#FF231F7C",
      });
    }
    return token;
  };
  return (
    <Block flex style={styles.event}>
      <AnimatedLoader
        visible={loading}
        overlayColor="rgba(255,255,255,0.75)"
        source={require("../../9764-loader.json")}
        animationStyle={{ width: 100, height: 100 }}
        speed={1}
      >
        <Text style={{ fontSize: 18, fontWeight: "bold" }}>Xin hãy đợi...</Text>
      </AnimatedLoader>
      <ImageBackground
        source={{ uri: BASE_URL_IMAGE + eventDetail.image }}
        style={styles.eventContainer}
        imageStyle={styles.eventImage}
        resizeMode="cover"
      >
        <Block flex style={styles.eventDetails}>
          <LinearGradient
            colors={["rgba(0,0,0,1)", "rgba(0,0,0,0)"]}
            style={styles.gradient}
          >
            <TouchableWithoutFeedback
              disabled={disabled}
              style={{
                justifyContent: "center",
                alignItems: "center",
                width: theme.SIZES.BASE * 4,
                height: theme.SIZES.BASE * 4,
              }}
              onPress={() => {
                Navigation.goBack();
                setdisabled(true);
              }}
            >
              <Icon
                name="arrow-back-ios"
                family="Material-Icons"
                color="white"
                size={20}
                style={{
                  alignSelf: "center",
                }}
              />
            </TouchableWithoutFeedback>
          </LinearGradient>
        </Block>
      </ImageBackground>
      <Block flex={0.7}>
        <Block style={styles.options}>
          <ScrollView vertical={true} showsVerticalScrollIndicator={false}>
            <Block
              row
              space="between"
              style={{ paddingVertical: 16, alignItems: "baseline" }}
            >
              <Text h4 bold muted>
                {eventDetail.name}
              </Text>
            </Block>
            {scan ? (
              <Block style={{ padding: theme.SIZES.BASE }}>
                <Block>
                  <Block>
                    <QRScan />
                  </Block>
                  <Block>
                    <Button
                      shadowless
                      color="danger"
                      style={[styles.button, styles.shadow]}
                      onPress={() => {
                        setloading(true);
                        setTimeout(() => {
                          setloading(false);
                          setscan(false);
                        }, 2000);
                      }}
                    >
                      HOÀN TẤT
                    </Button>
                  </Block>
                </Block>
              </Block>
            ) : (
              <>
                <Block space="around" style={{ flexWrap: "wrap" }}>
                  <Block row style={styles.detail}>
                    <MaterialCommunityIcons
                      name="information-outline"
                      color={theme.COLORS.MUTED}
                      size={16}
                    />
                    <Text
                      muted
                      size={16}
                      style={{ paddingHorizontal: theme.SIZES.BASE / 2 }}
                    >
                      Trạng thái: {eventDetail.status}
                    </Text>
                  </Block>
                  <Block row style={styles.detail}>
                    <Icon
                      name="description"
                      family="Material-Icons"
                      color={theme.COLORS.MUTED}
                      size={16}
                    />
                    <Text
                      muted
                      size={16}
                      style={{ paddingHorizontal: theme.SIZES.BASE / 2 }}
                    >
                      Mô tả: {eventDetail.description}
                    </Text>
                  </Block>
                  <Block row style={styles.detail}>
                    <Icon
                      name="lock-clock"
                      family="Material-Icons"
                      color={theme.COLORS.MUTED}
                      size={16}
                    />
                    <Text
                      muted
                      size={16}
                      style={{ paddingHorizontal: theme.SIZES.BASE / 2 }}
                    >
                      Thời gian đăng ký:{" "}
                      {moment(eventDetail.startTimeRegister).format(
                        "HH:mm DD-MM-yyy"
                      )}{" "}
                      -{" "}
                      {moment(eventDetail.endTimeRegister).format(
                        "HH:mm DD-MM-yyy"
                      )}
                    </Text>
                  </Block>
                  <Block row style={styles.detail}>
                    <Icon
                      name="clock"
                      family="font-awesome-5"
                      color={theme.COLORS.MUTED}
                      size={16}
                    />
                    <Text
                      muted
                      size={16}
                      style={{ paddingHorizontal: theme.SIZES.BASE / 2 }}
                    >
                      Thời gian:{" "}
                      {moment(eventDetail.startTime).format("HH:mm DD-MM-yyy")}{" "}
                      - {moment(eventDetail.endTime).format("HH:mm DD-MM-yyy")}
                    </Text>
                  </Block>
                  <Block row style={styles.detail}>
                    <Icon
                      name="map-marked-alt"
                      family="font-awesome-5"
                      color={theme.COLORS.MUTED}
                      size={16}
                    />
                    <Text
                      muted
                      size={16}
                      style={{ paddingHorizontal: theme.SIZES.BASE / 2 }}
                    >
                      Địa điểm: {eventDetail.address}
                    </Text>
                  </Block>
                </Block>
                <Block style={{ padding: theme.SIZES.BASE }}>
                  <Block>
                    {route.params.status === "Đang diễn ra" ? (
                      <Button
                        shadowless
                        color="info"
                        style={[styles.button, styles.shadow]}
                        onPress={() => {
                          setloading(true);
                          setTimeout(() => {
                            setloading(false);
                            setscan(true);
                          }, 2000);
                        }}
                      >
                        QUÉT MÃ ĐIỂM DANH
                      </Button>
                    ) : null}
                  </Block>
                </Block>
              </>
            )}
            {/* <Button
              shadowless
              color="info"
              style={[styles.button, styles.shadow]}
              onPress={() => {
                handleGetTokenNotifications()
                  .then((token) => {
                    // console.log("token 1 ", token);
                    pushNotification(token, "Thông báo!", "Test");
                  })
                  .catch((err) => {
                    Alert.alert("ERROR", err);
                  });
              }}
            >
              Test
            </Button> */}
          </ScrollView>
        </Block>
      </Block>
    </Block>
  );
};

export default EventDetailAdmin;
