import { useIsFocused, useNavigation } from "@react-navigation/core";
import { LinearGradient } from "expo-linear-gradient";
import { Block, Button, Text, theme } from "galio-framework";
import moment from "moment";
import queryString from "querystring";
import React, { useEffect, useState } from "react";
import {
  DevSettings,
  Dimensions,
  ImageBackground,
  ScrollView,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { Icon } from "../../../components";
import { materialTheme } from "../../../constants";
import eventJoinActions from "../../Redux/Actions/eventJoinActions";
import userActions from "../../Redux/Actions/userActions";
import { BASE_URL_IMAGE } from "../../Service/ServiceURL";
import styles from "./styles";
import _ from "lodash";
import AnimatedLoader from "react-native-animated-loader";
import teamActions from "../../Redux/Actions/teamActions";

const { width } = Dimensions.get("screen");
const Profile = () => {
  //!Const
  const detail = useSelector((state) => state.userReducer.data);
  const forcused = useIsFocused();
  const token = useSelector(
    (state) => state.userReducer.data.tokens.refresh.token
  );
  const listTeam = useSelector((state) => state.teamReducer.listTeam);
  const listTeamUser = [];
  const Navigation = useNavigation();
  const dispatch = useDispatch();
  const userByID = useSelector((state) => state.userReducer.userByID);
  const idUser = useSelector((state) => state.userReducer.data.user.id);
  const listEventJoin = useSelector(
    (state) => state.eventJoinReducer.listEventJoin
  );
  //!State
  const [userDetail, setuserDetail] = useState({});
  const [loading, setloading] = useState(false);
  const [stringTeam, setstringTeam] = useState("");
  //!Use effect
  useEffect(() => {
    if (forcused) {
      dispatch(userActions.getUsers(idUser));
      dispatch(
        teamActions.getListTeam(queryString.stringify({ limit: 100000 }))
      );
      dispatch(
        eventJoinActions.getListEventJoin(
          queryString.stringify({
            userId: idUser,
            populate: "eventId,userId",
            status: "Đã Tham gia",
          })
        )
      );
    }
  }, [forcused]);
  useEffect(() => {
    setuserDetail(userByID);
  }, [userByID]);

  useEffect(() => {
    let string = "";
    listTeamUser.splice(0, listTeamUser.length);
    listTeam.map((index) => {
      if (userByID.teamID !== undefined) {
        userByID.teamID.map((index1) => {
          if (index.id === index1) {
            listTeamUser.push(index.name);
          }
        });
      }
    });
    for (let i = 0; i < listTeamUser.length; i++) {
      if (i < listTeamUser.length - 1) {
        string = string + listTeamUser[i] + ", ";
      } else {
        string = string + listTeamUser[i];
      }
    }
    setstringTeam(string);
  }, [listTeam, userByID]);

  //!Function
  const onLogout = () => {
    setloading(true);
    dispatch({ type: "_REQUEST" });
    dispatch(
      userActions.logout(queryString.stringify({ refreshToken: token }), {
        success: () => {
          // if (_.isEmpty(detail)) {
          //
          setTimeout(() => {
            DevSettings.reload();
          }, 0);
          // }
        },
        failed: (mess) => {
          DevSettings.reload();
        },
      })
    );
  };
  return (
    <Block flex style={styles.profile}>
      <AnimatedLoader
        visible={loading}
        overlayColor="rgba(255,255,255,0.75)"
        source={require("../../9764-loader.json")}
        animationStyle={{ width: 100, height: 100 }}
        speed={1}
      >
        <Text style={{ fontSize: 18, fontWeight: "bold" }}>Xin hãy đợi...</Text>
      </AnimatedLoader>
      <ImageBackground
        source={{ uri: BASE_URL_IMAGE + userDetail.image }}
        style={styles.profileContainer}
        imageStyle={styles.profileImage}
        resizeMode="cover"
      >
        {/* <TouchableWithoutFeedback
          style={{
            justifyContent: "center",
            alignItems: "center",
            width: theme.SIZES.BASE * 4,
            height: theme.SIZES.BASE * 4,
          }}
          // onPress={() => {
          //   Navigation.goBack();
          // }}
        >
          <Icon
            name="arrow-back-ios"
            family="Material-Icons"
            color="white"
            size={20}
            style={{
              alignSelf: "center",
            }}
          />
        </TouchableWithoutFeedback> */}
        <Block flex style={styles.profileDetails}>
          <Block style={styles.profileTexts}>
            <Text color="white" size={28} style={{ paddingBottom: 8 }}>
              {userDetail.name}
            </Text>
            <Block row space="between">
              <Block row>
                <Block middle style={styles.pro}>
                  <Text size={16} color="white">
                    {userDetail.role}
                  </Text>
                </Block>
                <Text color="white" size={16} muted style={styles.seller}>
                  {userDetail.studentCode}
                </Text>
                {/* <Text size={16} color={materialTheme.COLORS.WARNING}>
                  4.8 <Icon name="shape-star" family="GalioExtra" size={14} />
                </Text> */}
              </Block>
              {/* <Block>
                <Text color={theme.COLORS.MUTED} size={16}>
                  <Icon
                    name="map-marker"
                    family="font-awesome"
                    color={theme.COLORS.MUTED}
                    size={16}
                  />
                  {`  `} Los Angeles, CA
                </Text>
              </Block> */}
            </Block>
          </Block>
          <LinearGradient
            colors={["rgba(0,0,0,0)", "rgba(0,0,0,1)"]}
            style={styles.gradient}
          />
        </Block>
      </ImageBackground>
      <Block flex={0.7}>
        <Block style={styles.options}>
          <ScrollView vertical={true} showsVerticalScrollIndicator={false}>
            <Block row space="between" style={{ padding: theme.SIZES.BASE }}>
              <Block middle>
                <Text bold size={20} style={{ marginBottom: 8 }}>
                  {/* {listEventJoin} */}
                  {listEventJoin.length}
                </Text>
                <Text muted size={14}>
                  Sự kiện đã tham gia
                </Text>
              </Block>
              <Block middle>
                <Text bold size={20} style={{ marginBottom: 8 }}>
                  {userDetail.totalScore}
                </Text>
                <Text muted size={14}>
                  Điểm hoạt động
                </Text>
              </Block>
              {/* <Block middle>
                <Text bold size={12} style={{ marginBottom: 8 }}>
                  2
                </Text>
                <Text muted size={12}>
                  Messages
                </Text>
              </Block> */}
            </Block>
            <Block
              row
              space="between"
              style={{ paddingVertical: 16, alignItems: "baseline" }}
            >
              <Text bold size={20} muted>
                THÔNG TIN TÀI KHOẢN
              </Text>
              {/* <Text
                size={12}
                color={theme.COLORS.PRIMARY}
                onPress={() => this.props.navigation.navigate("Home")}
              >
                View All
              </Text> */}
            </Block>
            <Block space="around" style={{ flexWrap: "wrap" }}>
              {/* {Images.Viewed.map((img, imgIndex) => (
                <Image
                  source={{ uri: img }}
                  key={`viewed-${img}`}
                  resizeMode="cover"
                  style={styles.thumb}
                />
              ))} */}
              <Block row style={styles.detail}>
                <Icon
                  name="transgender"
                  family="font-awesome"
                  color={theme.COLORS.MUTED}
                  size={16}
                />
                <Text
                  muted
                  size={16}
                  style={{ paddingHorizontal: theme.SIZES.BASE / 2 }}
                >
                  {" "}
                  {userDetail.gender}
                </Text>
              </Block>
              <Block row style={styles.detail}>
                <Icon
                  name="birthday-cake"
                  family="font-awesome"
                  color={theme.COLORS.MUTED}
                  size={16}
                />
                <Text
                  muted
                  size={16}
                  style={{ paddingHorizontal: theme.SIZES.BASE / 2 }}
                >
                  {moment(userDetail.dateOfBirth).format("DD/MM/YYYY")}
                </Text>
              </Block>
              <Block row style={styles.detail}>
                <Icon
                  name="phone"
                  family="font-awesome"
                  color={theme.COLORS.MUTED}
                  size={16}
                />
                <Text
                  muted
                  size={16}
                  style={{ paddingHorizontal: theme.SIZES.BASE / 2 }}
                >
                  {" "}
                  {userDetail.phoneNumber}
                </Text>
              </Block>

              <Block row style={styles.detail}>
                <Icon
                  name="email"
                  family="Material-Icons"
                  color={theme.COLORS.MUTED}
                  size={16}
                />
                <Text
                  muted
                  size={16}
                  style={{ paddingHorizontal: theme.SIZES.BASE / 2 }}
                >
                  {userDetail.email}
                </Text>
              </Block>

              <Block row style={styles.detail}>
                <Icon
                  name="class"
                  family="Material-Icons"
                  color={theme.COLORS.MUTED}
                  size={16}
                />
                <Text
                  muted
                  size={16}
                  style={{ paddingHorizontal: theme.SIZES.BASE / 2 }}
                >
                  {userDetail.class} - K{userDetail.schoolYear}
                </Text>
              </Block>
              <Block row style={styles.detail}>
                <Icon
                  name="fact-check"
                  family="Material-Icons"
                  color={theme.COLORS.MUTED}
                  size={16}
                />
                <Text
                  muted
                  size={16}
                  style={{ paddingHorizontal: theme.SIZES.BASE / 2 }}
                >
                  {userDetail.faculty}
                </Text>
              </Block>
              <Block row style={styles.detail}>
                <Icon
                  name="group"
                  family="font-awesome"
                  color={theme.COLORS.MUTED}
                  size={16}
                />
                <Text
                  muted
                  size={16}
                  style={{ paddingHorizontal: theme.SIZES.BASE / 2 }}
                >
                  {stringTeam}
                </Text>
              </Block>
              <Block row style={styles.detail}>
                <Icon
                  name="home-work"
                  family="Material-Icons"
                  color={theme.COLORS.MUTED}
                  size={16}
                />
                <Text
                  muted
                  size={16}
                  style={{ paddingHorizontal: theme.SIZES.BASE / 2 }}
                >
                  {userDetail.address}
                </Text>
              </Block>
            </Block>

            <Block style={{ padding: theme.SIZES.BASE }}>
              <Block>
                <Button
                  shadowless
                  color={materialTheme.COLORS.MICHAEL_COLOR}
                  style={[styles.button, styles.shadow]}
                  // onPress={Navigation.navigate("UpdateDetailUser")}
                  onPress={() => {
                    Navigation.navigate("UpdateDetailUser", userDetail);
                  }}
                >
                  CẬP NHẬT THÔNG TIN
                </Button>
              </Block>
              <Block>
                <Block>
                  <Button
                    shadowless
                    color="info"
                    style={[styles.button, styles.shadow]}
                    onPress={() => {
                      Navigation.navigate("ChangePassword");
                    }}
                  >
                    ĐỔI MẬT KHẨU
                  </Button>
                </Block>
                <Block>
                  <Button
                    shadowless
                    color="danger"
                    style={[styles.button, styles.shadow]}
                    onPress={
                      //   () => {
                      //   // Navigation.navigate("Login");
                      //   dispatch({ type: "_REQUEST" });
                      //   dispatch(userActions.logout());
                      // }
                      onLogout
                    }
                  >
                    ĐĂNG XUẤT
                  </Button>
                </Block>
              </Block>
            </Block>
            {/* <ImageUpload avatar={userDetail.avatar} /> */}
          </ScrollView>
        </Block>
      </Block>
    </Block>
  );
};

export default Profile;
