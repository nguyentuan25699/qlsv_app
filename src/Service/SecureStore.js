// import SecureStorage, {
//   ACCESS_CONTROL,
//   ACCESSIBLE,
//   AUTHENTICATION_TYPE,
// } from "expo-secure-store";
// import * as SecureStore from "expo-secure-store";
import AsyncStorage from "@react-native-async-storage/async-storage";

export async function SetItem(key, value) {
  // const config = {
  //   accessControl: ACCESS_CONTROL.BIOMETRY_ANY_OR_DEVICE_PASSCODE,
  //   accessible: ACCESSIBLE.WHEN_UNLOCKED,
  //   authenticationPrompt: "auth with yourself",
  //   service: "example",
  //   authenticateType: AUTHENTICATION_TYPE.BIOMETRICS,
  // };
  //   const key = "someKey";
  await AsyncStorage.setItem(key, value);
  //   const got = await SecureStorage.getItem(key, config);
  //   console.log(got);
}

export async function GetItem(key) {
  // const config = {
  //   accessControl: ACCESS_CONTROL.BIOMETRY_ANY_OR_DEVICE_PASSCODE,
  //   accessible: ACCESSIBLE.WHEN_UNLOCKED,
  //   authenticationPrompt: "auth with yourself",
  //   service: "example",
  //   authenticateType: AUTHENTICATION_TYPE.BIOMETRICS,
  // };
  let result = await AsyncStorage.getItem(key);
  console.log({ result });
  return result;
}

export default { SetItem, GetItem };
