export const BASE_URL = "https://qlhd.haui.bfd.vn/api/v1/";
export const BASE_URL_LOG_OUT = "https://qlhd.haui.bfd.vn/api/v1/auth/logout";
export const BASE_URL_IMAGE = "https://qlhd.haui.bfd.vn/api";

//!User
export const Login = "auth/login";
export const User = "users";
export const ChangePass = "auth/change-password";
export const ForgotPass = "auth/forgot-password";
//!Event
export const Event = "event";

//!
export const Team = "team";

//!Upload
const Upload = "upload";

export const Product = "product";
export const ProductByUser = "product/getProductByUser";
export const ProductType = "product/getProductType";
export const Unit = "product/getProductUnit";
export const Level = "product/getProductLevel";

export const UploadImage = "upload";
//!Event Join
export const EventJoin = "eventJoin";
export const joinEvent = "eventJoin/join";
export const registerEvent = "eventJoin/register";

export const getDoor = "door";
export const drawDoor = "door/draw";
//!Notification
export const notificationByUser = "notification/byUser";
export const notification = "notification";

export default {
  BASE_URL,
  BASE_URL_LOG_OUT,
  BASE_URL_IMAGE,
  Login,
  User,
  ChangePass,
  Event,
  ForgotPass,

  EventJoin,
  joinEvent,
  registerEvent,

  Team,

  Upload,
  notificationByUser,
  notification,

  Product,
  ProductByUser,
  ProductType,
  Unit,
  Level,

  UploadImage,

  getDoor,
  drawDoor,
};
