import AsyncStorage from "@react-native-async-storage/async-storage";
import { applyMiddleware, compose, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import { createLogger } from "redux-logger";
import persistReducer from "redux-persist/es/persistReducer";
import persistStore from "redux-persist/es/persistStore";
import createMiddlewareSaga from "redux-saga";
import rootReducers from "../Redux/Reducer/index";
import rootSagas from "../Redux/Saga/index";
import TypeActions from "../Redux/TypeActions";
import { removeAuth, removeItemValue, setToken } from "../Service/axiosClient";
import _ from "lodash";

//* Middleware: Redux Persist Config
const persistConfig = {
  //* Root
  key: "root",
  timeout: 0,
  //* Storage Method (React Native)
  storage: AsyncStorage,
  //* Whitelist (Save Specific Reducers)
  whitelist: ["userReducer"],
  //* Blacklist (Don't Save Specific Reducers)
  blacklist: [],
};
// setup middleware
const middleware = [];
const sagaMiddleware = createMiddlewareSaga();
const handleAuthTokenMiddleware = () => (next) => (action) => {
  if (action.type === TypeActions.LOGIN_REQUEST_SUCCESS) {
    const token = action.data.tokens.access.token;

    setToken(token);
  }
  if (action.type === TypeActions.LOG_OUT) {
    // removeAuth();
    setToken("");
  }
  next(action);
};
// const middleware = [sagaMiddleware, createLogger()];
middleware.push(sagaMiddleware, handleAuthTokenMiddleware);
if (__DEV__) {
  middleware.push(createLogger());
}

// const store = createStore(
//   rootReducers,
//   composeWithDevTools(applyMiddleware(...middleware))
// );

const persistedReducer = persistReducer(persistConfig, rootReducers);
const enhancers = [applyMiddleware(...middleware)];

const config = { enhancers };

const store = createStore(persistedReducer, undefined, compose(...enhancers));
//* Middleware: Redux Persist Persister
const persistor = persistStore(store, config, () => {
  const stateData = store.getState();
  if (
    !_.isEmpty(stateData.userReducer) &&
    !_.isEmpty(stateData.userReducer.data.tokens)
  ) {
    setToken(stateData.userReducer.data.tokens.access.token);
    return;
  }
});

sagaMiddleware.run(rootSagas);

export { store, persistor };
