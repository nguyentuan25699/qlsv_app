import _ from "lodash";
import { call, put, takeLatest } from "redux-saga/effects";
import TypeActions from "../TypeActions/index";
import ServiceURL from "../../Service/ServiceURL";
import { DELETE, GET, PATCH, POST } from "../../Service/ServiceBase";

export function* getListNotification(data) {
  const url = ServiceURL.notificationByUser + "?" + data.params;
  const callback = data.callback;
  try {
    const res = yield call(GET, url);
    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.GET_LIST_NOTIFICATION_FAILED,
        error: res.message,
      });
      callback && callback.failed(res.message);
    } else {
      yield put({
        type: TypeActions.GET_LIST_NOTIFICATION_SUCCESS,
        data: res.data.results,
      });
      callback && callback.success(res.data);
    }
  } catch (error) {
    yield put({ type: TypeActions.GET_LIST_NOTIFICATION_FAILED, error });
    callback && callback.failed(error.data.response.message);
  }
}
export function* createNotification(data) {
  const url = ServiceURL.notification;
  try {
    const res = yield call(POST, url, data.body);
    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.CREATE_NOTIFICATION_FAILED,
        error: res.message,
      });
    } else {
      yield put({
        type: TypeActions.CREATE_NOTIFICATION_SUCCESS,
        // data: res.data.results,
      });
    }
  } catch (error) {
    yield put({ type: TypeActions.CREATE_NOTIFICATION_FAILED, error });
  }
}

export default function* notificationSaga() {
  yield takeLatest(
    TypeActions.GET_LIST_NOTIFICATION_REQUEST,
    getListNotification
  );
  yield takeLatest(TypeActions.CREATE_NOTIFICATION_REQUEST, createNotification);
}
