import TypeActions from "../TypeActions/index";

export const uploadImage = (body, callback) => {
  return {
    type: TypeActions.UPLOAD_IMAGE_REQUEST,
    body,
    callback,
  };
};

export default { uploadImage };
