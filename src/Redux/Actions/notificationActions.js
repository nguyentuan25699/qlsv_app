import TypeActions from "../TypeActions";

export const getListNotification = (params, callback) => {
  return {
    type: TypeActions.GET_LIST_NOTIFICATION_REQUEST,
    params,
    callback,
  };
};

export const createNotification = (body) => {
  return {
    type: TypeActions.CREATE_NOTIFICATION_REQUEST,
    body,
    // callback,
  };
};

export const updateNotification = (body, params, callback) => {
  return {
    type: TypeActions.UPDATE_NOTIFICATION_REQUEST,
    body,
    params,
    callback,
  };
};

export const deleteNotification = (params, callback) => {
  return {
    type: TypeActions.DELETE_NOTIFICATION_REQUEST,
    params,
    callback,
  };
};

export default {
  getListNotification,
  createNotification,
  updateNotification,
  deleteNotification,
};
