import TypeActions from "Redux/TypeActions";

export const getListProduct = (callback) => {
  return {
    type: TypeActions.GET_LIST_PRODUCT_REQUEST,
    callback,
  };
};

export const getListProductByUser = (callback) => {
  return {
    type: TypeActions.GET_LIST_PRODUCT_BY_USER_REQUEST,
    callback,
  };
};

export const getListTypeProduct = () => {
  return {
    type: TypeActions.GET_LIST_TYPE_PRODUCT_REQUEST,
  };
};

export const getListUnitProduct = () => {
  return {
    type: TypeActions.GET_LIST_UNIT_PRODUCT_REQUEST,
  };
};

export const getListLevelProduct = () => {
  return {
    type: TypeActions.GET_LIST_LEVEL_PRODUCT_REQUEST,
  };
};

export const createProduct = (body, callback) => {
  return {
    type: TypeActions.CREATE_PRODUCT_REQUEST,
    callback,
    body,
  };
};

export const getProduct = (callback) => {
  return {
    type: TypeActions.GET_PRODUCT_REQUEST,
    callback,
  };
};

export const deleteProduct = (callback) => {
  return {
    type: TypeActions.DELETE_PRODUCT_REQUEST,
    callback,
  };
};

export const updateProduct = (body, callback) => {
  return {
    type: TypeActions.UPDATE_PRODUCT_REQUEST,
    body,
    callback,
  };
};

export default {
  getListProduct,
  getListLevelProduct,
  getListUnitProduct,
  getListTypeProduct,
  createProduct,
  getProduct,
  deleteProduct,
  updateProduct,
  getListProductByUser,
};
