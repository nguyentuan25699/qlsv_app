import TypeActions from "../TypeActions/index";

const initialState = {
  isGetting: false,
  listDoor: [],
  isDrawing: false,
  drawDoor: "",
};

export const doorReducer = (state = initialState, action) => {
  switch (action.type) {
    //!Get Door
    case TypeActions.GET_DOOR_REQUEST:
      return {
        ...state,
        isGetting: true,
      };
    case TypeActions.GET_DOOR_SUCCESS:
      return {
        ...state,
        isGetting: false,
        listDoor: action.data,
      };
    case TypeActions.GET_DOOR_FAILED:
      return {
        ...state,
        isGetting: false,
        error: action.error,
      };

    //!Draw Door
    case TypeActions.DRAW_DOOR_REQUEST:
      return {
        ...state,
        isDrawing: true,
      };
    case TypeActions.DRAW_DOOR_SUCCESS:
      return {
        ...state,
        isDrawing: false,
        drawDoor: action.data,
      };
    case TypeActions.DRAW_DOOR_FAILED:
      return {
        ...state,
        isDrawing: false,
        error: action.error,
      };

    //!Default
    default:
      return {
        ...state,
      };
  }
};
export default doorReducer;
