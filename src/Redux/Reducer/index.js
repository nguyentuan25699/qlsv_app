import { combineReducers } from "redux";
import { userReducer } from "./userReducer";
import { productReducer } from "./productReducer";
import { doorReducer } from "./doorReducer";
import { eventReducer } from "./eventReducer";
import { uploadReducer } from "./uploadReducer";
import { teamReducer } from "./teamReducer";
import { eventJoinReducer } from "./eventJoinReducer";
import { notificationReducer } from "./notificationReducer";

const rootReducers = combineReducers({
  userReducer,
  productReducer,
  doorReducer,
  eventReducer,
  uploadReducer,
  teamReducer,
  eventJoinReducer,
  notificationReducer,
});

export default rootReducers;
