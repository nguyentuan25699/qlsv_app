import TypeActions from "../TypeActions/index";

const initialState = {
  listProduct: {},
  isGettingUserList: false,
  isgetting: false,
  isDeleting: false,
  iscreatting: false,
  error: "",
  typeProduct: [],
  unitProduct: [],
  levelProduct: [],
  productbyID: {},
  isUpdating: false,
};
export const productReducer = (state = initialState, action) => {
  switch (action.type) {
    //!Get list product
    case TypeActions.GET_LIST_PRODUCT_REQUEST:
      return {
        ...state,
        isGettingUserList: true,
      };
    case TypeActions.GET_LIST_PRODUCT_SUCCESS:
      return {
        ...state,
        isGettingUserList: false,
        listProduct: action.data,
      };
    case TypeActions.GET_LIST_PRODUCT_FAILED:
      return {
        ...state,
        isGettingUserList: false,
        error: action.error,
      };
    //!Get list product by user
    case TypeActions.GET_LIST_PRODUCT_BY_USER_REQUEST:
      return {
        ...state,
        isGettingUserList: true,
      };
    case TypeActions.GET_LIST_PRODUCT_BY_USER_SUCCESS:
      return {
        ...state,
        isGettingUserList: false,
      };
    case TypeActions.GET_LIST_PRODUCT_BY_USER_FAILED:
      return {
        ...state,
        isGettingUserList: false,
        error: action.error,
      };
    //!Get list type product
    case TypeActions.GET_LIST_TYPE_PRODUCT_REQUEST:
      return {
        ...state,
        isgetting: true,
      };
    case TypeActions.GET_LIST_TYPE_PRODUCT_SUCCESS:
      return {
        ...state,
        typeProduct: action.data,
        isgetting: false,
      };
    case TypeActions.GET_LIST_TYPE_PRODUCT_FAILED:
      return {
        ...state,
        isgetting: false,
      };
    //!Get list unit product
    case TypeActions.GET_LIST_UNIT_PRODUCT_REQUEST:
      return {
        ...state,
        isgetting: true,
      };
    case TypeActions.GET_LIST_UNIT_PRODUCT_SUCCESS:
      return {
        ...state,
        unitProduct: action.data,
        isgetting: false,
      };
    case TypeActions.GET_LIST_UNIT_PRODUCT_FAILED:
      return {
        ...state,
        isgetting: false,
      };
    //!Get list level product
    case TypeActions.GET_LIST_LEVEL_PRODUCT_REQUEST:
      return {
        ...state,
        isgetting: true,
      };
    case TypeActions.GET_LIST_LEVEL_PRODUCT_SUCCESS:
      return {
        ...state,
        levelProduct: action.data,
        isgetting: false,
      };
    case TypeActions.GET_LIST_LEVEL_PRODUCT_FAILED:
      return {
        ...state,
        isgetting: false,
      };
    //!Create level product
    case TypeActions.CREATE_PRODUCT_REQUEST:
      return {
        ...state,
        iscreatting: true,
      };
    case TypeActions.CREATE_PRODUCT_SUCCESS:
      return {
        ...state,
        productbyID: action.data,
        iscreatting: false,
      };
    case TypeActions.CREATE_PRODUCT_FAILED:
      return {
        ...state,
        iscreatting: false,
      };
    //!Get product by id
    case TypeActions.GET_PRODUCT_REQUEST:
      return {
        ...state,
        isgetting: true,
      };
    case TypeActions.GET_PRODUCT_SUCCESS:
      return {
        ...state,
        isgetting: false,
        productbyID: action.data,
      };
    case TypeActions.GET_PRODUCT_FAILED:
      return {
        ...state,
        isgetting: false,
      };

    //!Delete Product
    case TypeActions.DELETE_PRODUCT_REQUEST:
      return {
        ...state,
        isDeleting: true,
      };
    case TypeActions.DELETE_PRODUCT_SUCCESS:
      return {
        ...state,
        isDeleting: false,
        productbyID: action.data,
      };
    case TypeActions.DELETE_PRODUCT_FAILED:
      return {
        ...state,
        isDeleting: false,
      };
    //!Update Product
    case TypeActions.UPDATE_PRODUCT_REQUEST:
      return {
        ...state,
        isUpdating: true,
      };
    case TypeActions.UPDATE_PRODUCT_SUCCESS:
      return {
        ...state,
        isUpdating: false,
        productbyID: action.data,
      };
    case TypeActions.UPDATE_PRODUCT_FAILED:
      return {
        ...state,
        isUpdating: false,
      };

    default:
      return { ...state };
  }
};
export default productReducer;
